import styled from "styled-components";

export const SectionContainer = styled.section`
  background: ${props => props.background || "#fff"};
  min-height: 100%;
`;
