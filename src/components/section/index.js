import React from "react";

import { SectionContainer } from "./styles";

const Section = ({ ...props }) => {
  return <SectionContainer {...props}>{props.children}</SectionContainer>;
};

export default Section;
