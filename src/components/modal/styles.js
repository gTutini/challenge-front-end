import styled from "styled-components";

export const ModalContainer = styled.div`
  display: ${props => (props.show ? "block" : "none")};
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.4);
`;

export const ModalContent = styled.div`
  border-radius: 0.5rem;
  background-color: #333;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
  max-width: 500px;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 0px 30px 0px rgba(121, 125, 122, 1);
  .close {
    color: #aaa;
    margin-left: auto;
    font-size: 28px;
    font-weight: bold;
    &:hover,
    &:focus {
      color: #b22222;
      text-decoration: none;
      cursor: pointer;
    }
  }
`;

export const ModalBody = styled.div`
  text-align: center;
  color: #fff;
`;
