import React from "react";

import { ModalContainer, ModalContent, ModalBody } from "./styles";

class Modal extends React.Component {
  render() {
    return (
      <>
        <ModalContainer show={this.props.show}>
          <ModalContent>
            <i onClick={this.props.close} className="fa fa-times close" />
            <ModalBody>{this.props.children}</ModalBody>
          </ModalContent>
        </ModalContainer>
      </>
    );
  }
}

export default Modal;
