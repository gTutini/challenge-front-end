import React from "react";

import { FooterContainer, TextBox } from "./styles";
import { Container } from "react-grid-system";

const Footer = () => {
  return (
    <>
      <Container id="address" component={FooterContainer}>
        <TextBox>
          <p>Alameda Santos, 1978</p>
          <p>6th floor - Jardim Paulista</p>
          <p>São Paulo - SP</p>
          <p>+55 11 3090 8500</p>
        </TextBox>
        <TextBox>
          <p>London - UK</p>
          <p>125 Kingsway</p>
          <p>London WC2B 6NH</p>
        </TextBox>
        <TextBox>
          <p>Lisbon - Portugal</p>
          <p>Rua Rodrigues Faria, 103</p>
          <p>4th floor</p>
          <p>Lisbon - Portugal</p>
        </TextBox>
        <TextBox>
          <p>Curitiba - PR</p>
          <p>R. Francisco Rocha, 198</p>
          <p>Batel - Curitiba - PR</p>
        </TextBox>
        <TextBox>
          <p>Buenos Aires - Argentina</p>
          <p>Esmeralda 950</p>
          <p>Buenos Aires B C1007</p>
        </TextBox>
      </Container>
    </>
  );
};

export default Footer;
