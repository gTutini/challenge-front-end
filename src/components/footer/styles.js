import styled from "styled-components";

export const FooterContainer = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: center;
  flex-wrap: wrap;
  margin: 100px 0 0;
`;

export const TextBox = styled.div`
  text-align: left;
  color: #fff;
  font-weight: 300;
  margin: 15px;
`;
