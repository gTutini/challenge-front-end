import styled from "styled-components";

export const BooksContainer = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  min-height: 100vh;
  flex-direction: column;
  padding: 40px 0;
`;

export const TextBox = styled.div`
  h1 {
    font-size: 30px;
    font-weight: 900;
  }
  p {
    font-size: 20px;
  }
`;

export const BooksWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  margin: 50px 0 0;
  .book {
    cursor: pointer;
    margin: 15px 50px;
  }
`;

export const ModalWrapper = styled.div`
  text-align: center;
  h1 {
    margin: 20px 0;
  }
  p {
    margin: 10px 0;
  }
  img {
    border: 5px solid #fff;
    box-shadow: 0px 0px 10px 1px #000000;
  }
`;
