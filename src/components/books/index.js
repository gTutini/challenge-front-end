import React from "react";

import Section from "../section";
import Loader from "../loader";
import Modal from "../modal";
import { Container } from "react-grid-system";
import api from "../../services/api";

import { BooksContainer, TextBox, BooksWrapper, ModalWrapper } from "./styles";

class Books extends React.Component {
  constructor(props) {
    super(props);
    this.close = this.closeModal.bind(this);
  }

  state = {
    books: [],
    loading: true,
    showModal: false,
    modalBook: null
  };

  closeModal = () => {
    document.body.style.overflow = "auto";
    this.setState({ showModal: false });
  };

  openModal = book => {
    document.body.style.overflow = "hidden";
    this.setState({ showModal: true, modalBook: book });
  };

  getBooks = async () => {
    const response = await api.get("volumes?q=HARRY%20POTTER&maxResults=8");

    const books = response.data.items;

    this.setState({ books, loading: false });
  };

  componentDidMount() {
    this.getBooks();
  }

  render() {
    const { books, loading, showModal, modalBook } = this.state;
    return (
      <>
        <Section id="books">
          <Container component={BooksContainer}>
            <TextBox>
              <h1>Books</h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Inventore
              </p>
              <p>aliquid excepturi necessitatibus laborum tempora!</p>
            </TextBox>
            <BooksWrapper>
              {loading ? (
                <Loader />
              ) : (
                books.map(book => (
                  <div
                    onClick={() => this.openModal(book)}
                    className="book"
                    key={book.id}
                  >
                    <img
                      src={book.volumeInfo.imageLinks.thumbnail}
                      alt="cover"
                    />
                  </div>
                ))
              )}
            </BooksWrapper>
          </Container>
          {modalBook ? (
            <Modal close={this.close} show={showModal}>
              <ModalWrapper>
                <img
                  src={modalBook.volumeInfo.imageLinks.thumbnail}
                  alt="cover-modal"
                />
                <h1>{modalBook.volumeInfo.title}</h1>
                <p>Por: {modalBook.volumeInfo.authors}</p>
                <p>
                  Publicado em:{" "}
                  {new Date(
                    modalBook.volumeInfo.publishedDate
                  ).toLocaleDateString()}
                </p>
                <div>
                  <i className="fa fa-quote-left" />
                  <p>{modalBook.volumeInfo.description}</p>
                  <i className="fa fa-quote-right" />
                </div>
              </ModalWrapper>
            </Modal>
          ) : null}
        </Section>
      </>
    );
  }
}

export default Books;
