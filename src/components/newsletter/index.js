import React from "react";

import Section from "../../components/section";
import { Container, Row, Col } from "react-grid-system";
import Footer from "../../components/footer";

import {
  NewsletterContainer,
  TextBox,
  InputWrapper,
  SocialMedia
} from "./styles";

import Facebook from "../../assets/facebook.png";
import Twitter from "../../assets/twitter.png";
import Google from "../../assets/google.png";
import Pinterest from "../../assets/pinterest.png";

class Newsletter extends React.Component {
  state = {
    email: ""
  };

  handlePayload = e => {
    const { email } = this.state;

    // eslint-disable-next-line
    const payload = {
      site: "Pixter Digital Books",
      email
    };
  };

  render() {
    return (
      <>
        <Section background="#232527" id="newsletter">
          <Container component={NewsletterContainer}>
            <TextBox>
              <h1>Keep in touch with us</h1>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veniam
              </p>
              <p>at quidem mollitia velit dolor libero.</p>
            </TextBox>
            <Container fluid component={InputWrapper}>
              <Row>
                <Col md={8} xl={10}>
                  <input
                    onChange={e => this.setState({ email: e.target.value })}
                    placeholder="Enter your email to update"
                  />
                </Col>
                <Col>
                  <button onClick={this.handlePayload} type="submit">
                    Submit
                  </button>
                </Col>
              </Row>
            </Container>
            <SocialMedia>
              <a href="#facebook">
                <img src={Facebook} alt="facebook" />
              </a>
              <a href="#twitter">
                <img src={Twitter} alt="twitter" />
              </a>
              <a href="#google">
                <img src={Google} alt="google" />
              </a>
              <a href="#pinterest">
                <img src={Pinterest} alt="pinterest" />
              </a>
            </SocialMedia>
            <Footer />
          </Container>
        </Section>
      </>
    );
  }
}

export default Newsletter;
