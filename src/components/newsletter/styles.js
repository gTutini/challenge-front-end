import styled from "styled-components";

export const NewsletterContainer = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  min-height: 100vh;
  flex-direction: column;
  padding: 40px 0;
`;

export const TextBox = styled.div`
  h1 {
    font-size: 30px;
    font-weight: 900;
    color: #eccd00;
    margin: 20px 0;
  }
  p {
    font-size: 20px;
    color: #676767;
  }
`;

export const InputWrapper = styled.div`
  margin: 20px 0;
  width: 90%;
  input {
    border-radius: 5px;
    border: none;
    font-size: 15px;
    padding: 20px;
    width: 100%;
    margin: 20px 0;
  }
  button {
    cursor: pointer;
    background: #fff;
    color: #333;
    width: 130px;
    height: 55px;
    border: none;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 15px;
    border-radius: 5px;
    margin: 20px 0;
  }
`;

export const SocialMedia = styled.div`
  a {
    margin: 15px;
  }
`;
