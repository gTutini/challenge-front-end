import React from "react";

import { LdsFacebook } from "./styles";

const Loader = ({ ...props }) => {
  return (
    <LdsFacebook {...props}>
      <div></div>
      <div></div>
      <div></div>
    </LdsFacebook>
  );
};

export default Loader;
