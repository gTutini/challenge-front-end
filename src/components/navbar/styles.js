import styled from "styled-components";

export const NavContainer = styled.nav`
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
  > div {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    height: 120px;
    img {
      margin-right: auto;
    }
  }
`;

export const NavMenu = styled.span`
  display: flex;
  justify-content: space-around;
  a {
    font-weight: bolder;
    font-size: 23px;
    margin: 2vw;
  }
`;

export const Hamburguer = styled.div`
  z-index: 9;
  color: ${props => (props.show ? "#fcdb00" : "#333")};
  i {
    font-size: 30px;
  }
`;

export const PushMenu = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  right: 0;
  height: 100vh;
  background: #333;
  width: ${props => (props.show ? "320px" : "0")};
  justify-content: flex-end;
  align-items: center;
  box-shadow: 0px 0px 30px 0px rgba(121, 125, 122, 1);
  transition: all 0.5s;
  span {
    display: ${props => (props.show ? "flex" : "none")};
    flex-direction: column;
    height: 50%;
    justify-content: space-between;
    text-align: right;
    margin: 10px;
    a {
      font-weight: 900;
      color: #fcdb00;
      font-size: 40px;
    }
  }
`;
