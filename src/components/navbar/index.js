import React from "react";

import { NavContainer, PushMenu, NavMenu, Hamburguer } from "./styles";
import Logo from "../../assets/logo.png";
import { Container, Hidden } from "react-grid-system";

class Navbar extends React.Component {
  state = {
    isOpen: false
  };

  toggleMenu = () => {
    let { isOpen } = this.state;
    isOpen = isOpen ? false : true;

    this.setState({ isOpen });
  };

  render() {
    const { isOpen } = this.state;
    return (
      <NavContainer>
        <Container>
          <img src={Logo} alt="logo" />
          <Hidden xs sm>
            <NavMenu>
              <a href="#books">Books</a>
              <a href="#newsletter">Newsletter</a>
              <a href="#address">Address</a>
            </NavMenu>
          </Hidden>
          <Hidden md lg xl>
            <Hamburguer show={isOpen} onClick={this.toggleMenu}>
              <i className="fa fa-bars" />
            </Hamburguer>
            <PushMenu show={isOpen}>
              <span>
                <a onClick={this.toggleMenu} href="#books">
                  Books
                </a>
                <a onClick={this.toggleMenu} href="#newsletter">
                  Newsletter
                </a>
                <a onClick={this.toggleMenu} href="#address">
                  Address
                </a>
              </span>
            </PushMenu>
          </Hidden>
        </Container>
      </NavContainer>
    );
  }
}

export default Navbar;
