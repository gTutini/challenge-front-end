import styled from "styled-components";

export const MainContainer = styled.div`
  min-height: 100vh;
  display: flex;
  flex-wrap: wrap;
  padding: 0 20px;
`;

export const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  margin: auto;
`;

export const TextBox = styled.div`
  flex-direction: column;
  text-align: left;
  justify-content: space-between;
  display: flex;
  flex: 1;
  margin: auto;
  .title {
    margin: 0 0 20px;
    h1 {
      font-weight: 900;
      font-size: 33px;
    }
  }
  .subtitle {
    margin: 0 0 20px;
    p {
      font-weight: 400;
      font-size: 21px;
      color: #313841;
    }
  }
  .text {
    p {
      color: #555555;
      font-weight: 300;
    }
  }
  .icons {
    margin: 30px 0 0;
    display: flex;
    justify-content: space-between;
    width: 180px;
    i {
      font-size: 40px;
    }
  }
`;
