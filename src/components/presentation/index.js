import React from "react";

import Section from "../../components/section";
import { Container, Visible } from "react-grid-system";
import Ipad from "../../assets/ipad.png";

import { MainContainer, Wrapper, TextBox } from "./styles";

const Presentation = () => {
  return (
    <>
      <Section background="#fcdb00">
        <Container component={MainContainer}>
          <TextBox className="textbox">
            <div className="title">
              <h1>Pixter Digital Books</h1>
            </div>
            <div className="subtitle">
              <p>Lorem ipsum dolor sit amet?</p>
              <p>consectetur elit, volupat.</p>
            </div>
            <div className="text">
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
              <p>
                At, nisi! Nostrum exercitationem labore, dolore voluptatum
                eveniet
              </p>
              <p>vitae temporibus qui fugit sed cupiditate. Mollitia</p>
              <p>necessitatibus saepe odit eaque! Similique quo debitis</p>
              <p>doloremque expedita neque deserunt eveniet eius.</p>
            </div>
            <div className="icons">
              <i className="fa fa-apple"></i>
              <i className="fa fa-android"></i>
              <i className="fa fa-windows"></i>
            </div>
          </TextBox>
          <Visible md lg xl>
            <Wrapper>
              <img src={Ipad} alt="ipad" />
            </Wrapper>
          </Visible>
        </Container>
      </Section>
    </>
  );
};

export default Presentation;
