import { createGlobalStyle } from "styled-components";

import "font-awesome/css/font-awesome.css";

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap');

* {
  box-sizing: border-box;
  padding: 0;
  margin: 0;
  outline: 0;
}

body, html, #root {
  font-family: 'Lato', sans-serif;
  text-rendering: optimizeLegibility !important;
  -webkit-font-smoothing: antialiased !important;
  height: 100%;
  width: 100%;
}

a {
  text-decoration: none;
  color: #333;
  cursor: pointer;
}
`;

export default GlobalStyle;
