import React from "react";

import Main from "../src/pages/main";
import GlobalStyle from "../src/styles/global";
import Navbar from "./components/navbar";
import { ScreenClassProvider } from "react-grid-system";

function App() {
  return (
    <ScreenClassProvider>
      <GlobalStyle />
      <Navbar />
      <Main />
    </ScreenClassProvider>
  );
}

export default App;
