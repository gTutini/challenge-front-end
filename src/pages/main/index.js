import React from "react";

import Presentation from "../../components/presentation";
import Books from "../../components/books";
import Newsletter from "../../components/newsletter";

const Main = () => {
  return (
    <>
      <Presentation />
      <Books />
      <Newsletter />
    </>
  );
};

export default Main;
